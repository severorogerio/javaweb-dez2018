package br.senai.sp.informatica;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.servlets.libs.WebUtilities;

@SuppressWarnings("serial")
@WebServlet("/CadastraCliente")
public class CadastraCliente extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String cabecalho = WebUtilities.loadFile(this, "/resources/header.html");
		String rodape = WebUtilities.loadFile(this, "/resources/footer.html");
		
		PrintWriter out = response.getWriter();
		
		String nome = request.getParameter("nome");
		String endereco = request.getParameter("endereco");
		String telefone = request.getParameter("telefone");
		String email = request.getParameter("email");

		if(nome.isEmpty() || endereco.isEmpty() || 
		   telefone.isEmpty() || email.isEmpty()) { // se falta informação -> erro
			out.print(cabecalho.replace("TITULO", "ERRO") + 
					"<p>Alguns dados não foram informados</p>" +
					rodape);
		} else { // ok
			out.print(cabecalho.replace("TITULO", "CADASTRO EFETUADO") +
					"<p><b>Nome</b>: "+ nome +"</p>" +
					"<p><b>End</b>: "+ endereco +"</p>" +
					"<p><b>Fone</b>: "+ telefone +"</p>" +
					"<p><b>E-Mail</b>: "+ email +"</p>" +
					rodape);
		}
	}
}
