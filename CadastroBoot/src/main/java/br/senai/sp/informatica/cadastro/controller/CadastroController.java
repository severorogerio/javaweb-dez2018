package br.senai.sp.informatica.cadastro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.senai.sp.informatica.cadastro.model.Cliente;

@Controller
public class CadastroController {
	@RequestMapping({"/", "index.html"})
	public ModelAndView inicio() {
		return new ModelAndView("index", "cliente", new Cliente());
	}
	
	@RequestMapping("/cadastrar")
	public ModelAndView cadastar(@ModelAttribute("cliente") Cliente cliente) {
		return new ModelAndView("listaCliente", "cliente", cliente);
	}
}
