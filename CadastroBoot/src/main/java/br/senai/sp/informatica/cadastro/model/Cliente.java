package br.senai.sp.informatica.cadastro.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cliente {
	private String nome;
	private String endereco;
	private String telefone;
	private String email;
}
